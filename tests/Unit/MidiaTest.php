<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use App\Http\Service\MidiaService;

class MidiaTest extends TestCase {

    public function testTresUltimasMidias() {
    	$midiaService = new MidiaService();
        $midias = $midiaService->ultimasMidiasCadastradas();
    	$this->assertEquals(3, count($midias));
    }

}
