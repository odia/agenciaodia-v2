@extends('welcome')
@section('body')
<!--
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div>
-->
<header id="gtco-header" class="gtco-cover gtco-cover-md" role="banner" style="background-image: url(images/img_bg_2.jpg)">
    <div class="overlay"></div>

    <div class="gtco-container">
        <div class="row">
            <div class="col-md-12 col-md-offset-0 text-left">
                <div class="row row-mt-15em">
                    <div class="col-md-7 mt-text" data-animate-effect="fadeInUp">
                        <h1>Explore nosso acervo com mais de 70 anos de conteúdo</h1>
                    </div>

                    <!-- FORMULÁRIO DE LOGIN -->
                    <div class="col-md-4 col-md-push-1" data-animate-effect="fadeInRight">
                        <div class="form-wrap">
                            <div class="tab">
                                <div class="tab-content">
                                    <div class="tab-content-inner active" data-content="signup">
                                        @guest
                                            <h3>ACESSO À CONTA</h3>
                                            <p>Preencha os campos abaixo com seus dados para acessar sua conta</p>
                                            <form action="autenticar" method="POST">
                                                @csrf

                                                <div class="row form-group">
                                                    <div class="col-md-12">
                                                        <label for="email">Email</label>
                                                        <input type="text" id="email" name="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{ old('email') }}" required autofocus>

                                                        @if ($errors->has('email'))
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $errors->first('email') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                
                                                <div class="row form-group">
                                                    <div class="col-md-12">
                                                        <label for="password">Senha</label>
                                                        <input type="password" id="senha" name="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                                        @if ($errors->has('password'))
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $errors->first('password') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="row form-group">
                                                    <div class="col-md-12">
                                                        <input type="submit" class="btn btn-primary btn-block" value="ENVIAR">
                                                        <!--
                                                        <button type="submit" class="btn btn-primary btn-block">
                                                            {{ __('Login') }}
                                                        </button>
                                                        -->
                                                    </div>
                                                </div>
                                            </form>
                                        @else
                                            Olá {{$user->nm_usuas}}, seja bem-vindo(a)!<br>
                                            <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('frm-logout').submit();">
                                                Logout
                                            </a>
                                            
                                            <form id="frm-logout" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                {{ csrf_field() }}
                                            </form>
                                        @endguest
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- FORMULÁRIO DE BUSCA -->
    <div class="container busca">
        <form class="form-inline" action="pesquisarMidiasPorTitulo" method="GET">
            <div class="col-md-8 col-md-offset-2">
                <div class="col-md-9 col-sm-9">
                    <div class="form-group">
                        <input type="text" class="form-control" id="email" name="titulo" placeholder="Busque aqui" required>
                    </div>
                </div>
                
                <div class="col-md-3 col-sm-3">
                    <button type="submit" class="btn btn-primary">BUSCAR</button>
                </div>
            </div>
        </form>
    </div>
</header>

<div class="gtco-section">
    <div class="gtco-container">
        <div class="row">
            <div class="col-md-12 text-center gtco-heading">
                <h2>Últimas Cadastradas</h2>
                <!-- <p>Dignissimos asperiores vitae velit veniam totam fuga molestias accusamus alias autem provident. Odit ab aliquam dolor eius.</p> -->
            </div>
        </div>
        
        <!-- ÚLTIMAS MÍDIAS -->
        <div class="row" id="fotos">
            @foreach($midias as $m)
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="fh5co-card-item image-popup" >
                        <a href="verfoto?foto={{$m->ds_midia_link}}&arq={{$m->nm_midia}}&nome={{$m->ds_midia_orig}}&desc={{$m->ds_midia_orig}}" id="{{$m->cd_midia}}" class="aa">
                            <figure>
                                <div class="overlay"><i class="icon-camera"></i></div>
                                <img src="{{$m->ds_midia_link}}" id="img{{$m->cd_midia}}" alt="Image" class="img-responsive">
                            </figure>
                        </a>

                        <div class="fh5co-text">
                            <h2>fotos</h2>
                            <span>
                            <p class="text-left" id="desc{{$m->cd_midia}}" style="height: 65px;">
                                {{ str_limit($m->ds_midia_orig, $limit = 95)}}
                            </p>
                            </span>

                            <span>
                            <p class="text-left" id="desc2{{$m->cd_midia}}">
                                {{$m->ds_midia_orig}}
                            </p>
                            </span>

                            @auth
                                <div>
                                    <a href="download?foto={{$m->ds_midia_link}}&nome={{$m->nm_midia}}">
                                        {{$m->qt_midia_klbit}}
                                        <i class="icon-download"></i>
                                    </a>
                                </div>
                            @endauth                            
                            <!-- <p><span class="btn btn-primary">Schedule a Trip</span></p> -->
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
@endsection
