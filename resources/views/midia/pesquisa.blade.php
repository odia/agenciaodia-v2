@extends('welcome')
@section('body')
<br>
<div class="container">
	<div class="row">
		<div class="col-sm-6">
			<form method="get" action="/pesquisarMidiasPorTitulo">
			  <input type="text" placeholder="Buscando título" name="buscar">
			  <button type="submit">Buscar</button>
			</form>
		</div>

		<div class="col-sm-6">
			@if ($resultados > 0) 
				<h3>{{$total}} resultados foram encontrados!!</h3> 
			@endif  
			@if ($resultados == 0)
				Nenhuma resultado foi encontrado! 
			@endif

			<ul class="list-group">
			@foreach ($midias as $m)
				<li class="list-group-item">{{$m->cd_midia}} - {{$m->ds_midia_titlo}}</li>
			@endforeach
			</ul>

			<hr>
			{{$midias->links()}}
		</div>
	</div>

	<a href="/">Home</a>
</div>
@stop