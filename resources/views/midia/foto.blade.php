@extends('welcome2')
@section('body')
<div class="gtco-section">
	<div class="gtco-container">
		<div class="row photo">
			<div class="col-md-12 voltar">
				<a href="{{$voltar}}">
					<button type="submit" class="btn btn-voltar">VOLTAR</button>
				</a>
			</div>

			<div class="col-md-8 col-sm-8">
				<img src="{{$urlFoto}}" class="img-responsive">
			</div>

			<div class="col-md-4 col-sm-4">
				<h3>{{$nomeFoto}}</h3>

				<p>{{$descFoto}}</p>
				<!-- <p>Dignissimos asperiores vitae velit veniam totam fuga molestias accusamus alias autem provident. Odit ab aliquam dolor eius.</p> -->
				@auth
					<a href="download?foto={{$urlFoto}}&nome={{$nomeArquivo}}">
						<button type="submit" class="btn btn-primary">BAIXAR</button>
					</a>
				@endauth

				<button type="submit" class="btn btn-info">CONTATO</button>	
			</div>
		</div>
	</div>
</div>
@stop