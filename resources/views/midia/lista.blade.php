@extends('welcome2')
@section('body')
<div class="gtco-section">
	<div class="gtco-container">
		<div class="row">
			<div class="col-md-8 gtco-heading">
				<h2>Explore nosso acervo <br>com mais de 70 anos <br>de conteúdo</h2>
				<!-- <p>Dignissimos asperiores vitae velit veniam totam fuga molestias accusamus alias autem provident. Odit ab aliquam dolor eius.</p> -->
			</div>
		</div>

		<div class="row">
			<!-- FILTROS DA BUSCA -->
			<div class="col-md-4 col-sm-4" id="filtros">
				<div class="busca-interna">
					<form class="form-inline" action="pesquisarMidia">
						<div class="col-md-12">
							<div class="form-group">
								<input type="text" class="form-control" id="text" name="titulo" placeholder="Busque aqui" value="{{$titulo}}" required>

								<!-- 
								<span class="input-group-btn">
                                    <button class="btn btn-danger" type="button">
                                        <i class="icon-search"></i>
                                    </button>
                                </span>
                            	-->
							</div>
						</div>

						<div class="col-md-12">
							<div class="form-group">
								<select class="form-control" id="conditionsselect1" name="produto">
					                  <option value="">Portal</option>
					                  <option value="2">O Dia</option>
					                  <option value="1">Meia Hora</option>
					            </select>
							</div>
						</div>

						<div class="col-md-12">
							<div class="form-group">
								<select class="form-control" id="conditionsselect1" name="tipo_midia">
					                 <option value="">Categoria</option>
					                 <option value="36">Arquivo</option>
					                 <option value="32">Imagem</option>
					                 <option value="38">Video</option>
					                 <option value="35">Audio</option>
					                 <option value="34">Embed</option>
					            </select>
							</div>
						</div>

						<div class="col-md-12">
							<div class="form-group">
								<input class="form-control" id="data_inicio" name="data_inicio" placeholder="Data Início" type="text"/>
							</div>
						</div>

						<div class="col-md-12">
							<div class="form-group">
								<input class="form-control" id="data_fim" name="data_fim" placeholder="Data Final" type="text"/>
							</div>
						</div>

						<div class="col-md-12 text-right">
							<button type="submit" id="buscar" class="btn btn-primary">BUSCAR</button>
						</div>
					</form>
				</div>
			</div>

			<!-- RESULTADOS DA BUSCA --->
			<div class="col-md-8 col-sm-8" id="resultados">
				<!-- PAGINAÇÃO PARA OS REGISTROS ANTERIORES -->
				@if ($page > 1)
					<div class="row text-center">
						<div class="col-md-12">
							<a href="pesquisarMidiasPorTitulo?titulo={{$titulo}}&page={{$page-1}}">
								<button type="submit" name='buscar' class="btn btn-primary">RESULTADOS ANTERIORES</button>
							</a>
						</div>
					</div>
				@endif

				<!-- TOTAL DE RESULTADOS -->
				<div class="row">
					<div class="col-md-12 header-resultados">
						<h2>{{$midias->count()}} Resultados de {{$total}}</h2>
						<!-- <p>Dignissimos asperiores vitae velit veniam totam fuga molestias accusamus alias autem provident. Odit ab aliquam dolor eius.</p> -->
					</div>
				</div>

				<!-- RESULTADOS -->
				<div class="row" id="midias">
					@foreach ($midias as $m)
						<div class="col-md-4 col-sm-6">
							<div class="fh5co-card-item image-popup">
								<figure>
									@if ($m->cd_tpmid == 32)
										<a href="verfoto?foto={{$m->ds_midia_link}}&arq={{$m->nm_midia}}&nome={{$m->ds_midia_orig}}&desc={{$m->ds_midia_orig}}" id="{{$m->cd_midia}}" id="{{$m->cd_midia}}" class="aa">
											<div class="overlay">
												<i class="icon-camera"></i>
											</div>

			                                <img src="{{$m->ds_midia_link}}" id="img{{$m->cd_midia}}" alt="Image" class="img-responsive">
			                            </a>
										<!--
										<div class="zoomViewport">
                							<div class="zoomContainer">
                								<div class="zoomTarget" data-targetsize="0.45" data-duration="600">
                									<img src="{{$m->ds_midia_link}}" alt="Image" class="img-responsive">
                								</div>
                							</div>
                						</div>
                						-->
									@endif

									@if ($m->cd_tpmid == 18 || $m->cd_tpmid == 38)
										<div class="overlay">
											<i class="icon-video"></i>
										</div>

										<img src="{{$m->ds_midia_link}}" alt="Image" class="img-responsive">
									@endif
									@if ($m->cd_tpmid == 8 || $m->cd_tpmid == 35)
										<div class="overlay">
											<i class="icon-microphone"></i>
										</div>
									@endif
									@if ($m->cd_tpmid == 6)
										<div class="overlay">
											<i class="icon-play"></i>
										</div>
									@endif
									@if ($m->cd_tpmid == 1)
										<div class="overlay">
											<i class="icon-file"></i>
										</div>
									@endif
								</figure>

								<div class="fh5co-text">
									<ul>
										<li>CÓDIGO: {{$m->cd_midia}}</li>
										<li>AUTOR: {{$m->ds_midia_credi_orig}}</li>
										@auth
											<li>
												<a href="download?foto={{$m->ds_midia_link}}&nome={{$m->nm_midia}}">
													{{$m->qt_midia_klbit}} <i class="icon-download"></i>
												</a>
											</li>
										@endauth
									</ul>
								</div>
							</div>
						</div>
					@endforeach
				</div>

				<!-- PAGINAÇÃO PARA OS PRÓXIMOS REGISTROS -->
				@if ($midias->count() == 12)
					<div class="row text-center">
						<div class="col-md-12">
							<a href="pesquisarMidiasPorTitulo?titulo={{$titulo}}&page={{$page+1}}">
								<button type="submit" class="btn btn-primary">MAIS RESULTADOS</button>
							</a>
						</div>
					</div>
				@endif
			</div>
		</div>
	</div>
</div>
@stop