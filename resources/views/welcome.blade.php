<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Agência O Dia</title>

        <!-- Animate.css -->
        <link rel="stylesheet" href="css/animate.css">
        <!-- Icomoon Icon Fonts-->
        <link rel="stylesheet" href="css/icomoon.css">
        <!-- Themify Icons-->
        <link rel="stylesheet" href="css/themify-icons.css">
        <!-- Bootstrap  -->
        <link rel="stylesheet" href="css/bootstrap.css">

        <!-- Magnific Popup -->
        <link rel="stylesheet" href="css/magnific-popup.css">

        <!-- Magnific Popup -->
        <link rel="stylesheet" href="css/bootstrap-datepicker.min.css">

        <!-- Owl Carousel  -->
        <link rel="stylesheet" href="css/owl.carousel.min.css">
        <link rel="stylesheet" href="css/owl.theme.default.min.css">

        <!-- Theme style  -->
        <link rel="stylesheet" href="css/style.css">

        <!-- Modernizr JS -->
        <script src="js/modernizr-2.6.2.min.js"></script>

        <!-- Bootstrap -->
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="fonts/font-awesome/css/font-awesome.css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            .preview {
               position:absolute;
               border:1px solid #FF0000;
               background:#333;
               padding:5px;
               /*display: none;*/
               color:#fff;
            }


            .aa {
                z-index: 2;
            };
        </style>
    </head>
    <body>
        <div class="gtco-loader" style="display: none;"></div>

        <div class="page">
            <a href="#" class="js-gtco-nav-toggle gtco-nav-toggle gtco-nav-white"><i></i></a>

            <nav class="gtco-nav" role="navigation">
                <div class="gtco-container">
                    <div class="row">
                        <div class="col-sm-2 col-xs-12">
                            <div id="gtco-logo"><a href="/"><img src="images/logo_agencia_odia.png"</a></div>
                        </div>
                        <div class="col-xs-10 text-right menu-1">
                            <ul>
                                <li><a href="#">AGENCIA O DIA</a></li>
                                <li><a href="#">EXPEDIENTE</a></li>
                                <li><a href="#">FALE CONOSCO</a></li>
                                <li><a href="#">CONDIÇÕES COMERCIAIS</a></li>
                                <!--
                                <li><a href="pricing.html"><span><i class="icon-lock"></i>ÁREA RESTRITA</span></a></li>
                                -->
                            </ul>   
                        </div>
                    </div>
                </div>
            </nav>
        
            @yield('body')

            <footer id="gtco-footer" role="contentinfo">
                <div class="gtco-container">
                    <div class="row row-p b-md">
                        <div class="col-md-3">
                            <div class="gtco-widget">
                                <h3>EDITORIAS</h3>
                                <ul class="gtco-footer-links">
                                    <li><a href="#">Lorem ipsum</a></li>
                                    <li><a href="#">Lorem ipsum</a></li>
                                    <li><a href="#">Lorem ipsum</a></li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="gtco-widget">
                                <h3>NOSSOS PRODUTOS</h3>
                                <ul class="gtco-footer-links">
                                    <li><a href="#">Lorem ipsum</a></li>
                                    <li><a href="#">Lorem ipsum</a></li>
                                    <li><a href="#">Lorem ipsum</a></li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="gtco-widget">
                                <h3>AGÊNCIA O DIA</h3>
                                <ul class="gtco-footer-links">
                                    <li><a href="#">Lorem ipsum</a></li>
                                    <li><a href="#">Lorem ipsum</a></li>
                                    <li><a href="#">Lorem ipsum</a></li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="gtco-widget">
                                <h3>QUERO SER CLIENTE</h3>
                                <ul class="gtco-footer-links">
                                    <li><a href="#">Lorem ipsum</a></li>
                                    <li><a href="#">Lorem ipsum</a></li>
                                    <li><a href="#">Lorem ipsum</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="row copyright">
                        <div class="col-md-12">
                            <p class="pull-left">
                                <small class="block">Copyright - Agência O DIA - Todos os direitos reservados</small> 
                            </p>

                            <p class="pull-right">
                                <ul class="gtco-social-icons pull-right">
                                    <li><a href="#"><img src="images/logo_odia.png"></a></li>
                                    <li><a href="#"><img src="images/logo_meia.png"></a></li>
                                </ul>
                            </p>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    
        <div class="gototop js-top">
            <a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
        </div>
        
        <!-- jQuery -->
        <script src="js/jquery.min.js"></script>
        <!-- jQuery Easing -->
        <script src="js/jquery.easing.1.3.js"></script>
        <!-- Bootstrap -->
        <script src="js/bootstrap.min.js"></script>
        <!-- Waypoints -->
        <script src="js/jquery.waypoints.min.js"></script>
        <!-- Carousel -->
        <script src="js/owl.carousel.min.js"></script>
        <!-- countTo -->
        <script src="js/jquery.countTo.js"></script>

        <!-- Stellar Parallax -->
        <script src="js/jquery.stellar.min.js"></script>

        <!-- Magnific Popup -->
        <!--
        <script src="js/jquery.magnific-popup.min.js"></script>
        <script src="js/magnific-popup-options.js"></script>
        -->

        <!-- Datepicker -->
        <script src="js/bootstrap-datepicker.min.js"></script>
        
        <!-- Main -->
        <script src="js/main.js"></script>

        <script src="js/imagepreview.min.js"></script>
        <!-- <script src="js/preview.js"></script> -->

        <!-- ZOOMOOZ -->
        <!-- <script type="text/javascript" src="js/jquery.zoomooz-helpers.js"></script>
        <script type="text/javascript" src="js/jquery.zoomooz-anim.js"></script>
        <script type="text/javascript" src="js/jquery.zoomooz-core.js"></script>
        <script type="text/javascript" src="js/jquery.zoomooz-zoomTarget.js"></script>
        <script type="text/javascript" src="js/jquery.zoomooz-zoomButton.js"></script>
        <script type="text/javascript" src="js/jquery.zoomooz-zoomContainer.js"></script>
        <script type="text/javascript" src="js/purecssmatrix.js"></script>
        <script type="text/javascript" src="js/sylvester.src.stripped.js"></script> -->
        

        <script>
            $(document).ready(function() {
                $(".overlay").click(function(evt) {
                    //$(this).zoomTo({targetsize:0.75, duration:600});
                    //evt.stopPropagation();
                    //$(".img-responsive").zoomTarget();
                    $(".overlay").zoomTarget();
                })
                
                $(".fh5co-text").each(function() {
                    $(this).children("span:last").hide()
                })

                $(".fh5co-text").mouseover(function() {
                    var id = $(this).attr("id")

                    var descLenght = $(this).children("span:last").text().length
                    
                    if (descLenght > 92) {
                        $(this).children("span:first").hide()
                        $(this).children("span:last").show()
                    }
                }).mouseout(function() {
                    var id = $(this).attr("id")

                   var descLenght = $(this).children("span:last").text().length
                    
                    if (descLenght > 92) {
                        $(this).children("span:first").show()
                        $(this).children("span:last").hide()
                    }
                });
                
                /*
                $(".aa").each(function() {
                    $(this).mouseover(function(event) {
                        var x = $(this).offset().left + $("#img"+$(this).attr("id")).width()
                        var y = $(this).offset().top

                        var pos = $(".aa:even").index(this)
                        
                        if (pos == 1) {
                            x -= $("#img"+$(this).attr("id")).width()
                            y += $("#img"+$(this).attr("id")).height()
                        }
                        
                        $("body").append("<p id='preview' style='border: 2px solid blue; border-radius: 5px;'><img src='"+$(this).attr("href")+"' alt='Image preview' width='400' height='230' /></p>")
                      
                        var posY = event.pageY-10
                        var posX = event.pageX+30
                        
                        $("#preview").css({
                            "position": "absolute",
                            "top": y + "px",
                            "left": x + "px",
                            "z-index": "10"
                        }).fadeIn()
                    })

                    $(this).mouseout(function() {
                        $("#preview").remove();
                    })
                })
                */
            })
        </script>
    </body>
</html>
