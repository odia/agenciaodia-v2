<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Midia extends Model {

	protected $table = 'midia';

	public $timestamps = false;

	//protected $guarded = ['cd_midia'];
}