<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable {

    use Notifiable;

    protected $table = 'usuas';
    protected $primaryKey = 'cd_usuas';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getAuthPassword() {
        return $this->password;
    }

    public function setPasswordAttribute($value) {
        $this->attributes['password'] = md5($value);
    }

    public function setAttribute($key, $value) {
        $isRememberTokenAttribute = $key == $this->getRememberTokenName();
        if (!$isRememberTokenAttribute) {
          parent::setAttribute($key, $value);
        }
    }

    public function validateCredentials(UserContract $user, array $credentials) {
        $plain = $credentials['password'];
        $hashed = $user->getAuthPassword();
 
        if (strlen($hashed) === 0) {
            return false;
        }
 
        return password_verify($plain, $hashed);
    }

}
