<?php

namespace App\Http\Service;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;

use App\Model\Midia;

class MidiaService {
	
	public $totalMidias;

	public function ultimasMidiasCadastradas() {
		$ultima_midia = Midia::orderBy('dt_midia_incl', 'desc')
							->limit(1)->get();

		$midias = Midia::where(['cd_tpmid' => '32', 'cd_midia_w' => '700', 'cd_midia_h' => '470'])
					->where('ds_midia_titlo', '<>', $ultima_midia[0]->ds_midia_titlo)
					->orderBy('dt_midia_incl', 'desc')
					->limit(3)->get();

		return $midias;
	}

	public function listarMidias() {
		return Midia::simplePaginate(10);
	}

	public function pesquisarMidiasPorTitulo($titulo) {
		//$where = ['cd_tpmid' => '32', 'ds_midia_titlo' => $titulo, 'cd_midia_w' => '700', 'cd_midia_h' => '470'];
		$where = ['cd_tpmid' => '32', 'ds_midia_tag_orig' => $titulo, 'cd_midia_w' => '700', 'cd_midia_h' => '470'];
		$this->verificarTotalMidias($where);

		$midias = Midia::where($where)
					//->orderBy('dt_midia_incl', 'desc')
					->simplePaginate(12);

		return $midias;
	}

	public function pesquisarMidias($parametros) {
		$where = array();
		
		if ($parametros['titulo']) {
			//$where['ds_midia_titlo'] = $parametros['titulo'];
			$where['ds_midia_tag_orig'] = $parametros['titulo'];
		}
		if (array_key_exists("produto", $parametros)) {
			$where['cd_jonal'] = $parametros['produto'];
		}
		if (array_key_exists("tipo_midia", $parametros)) {
			$where['cd_tpmid'] = $parametros['tipo_midia'];
			/*
			if ($parametros['tipo_midia'] == '2') {
				$where['cd_midia_w'] = '700';
				$where['cd_midia_h'] = '470';
			}
			*/
		}

		$where['cd_midia_w'] = '700';
		$where['cd_midia_h'] = '470';

		$this->verificarTotalMidias($where);
		$midias = null;
		if (array_key_exists("data_inicio", $parametros)) {
			$midias = Midia::where($where)
						//->where(['cd_midia_h' => '80', 'cd_midia_w' => '80'])
						->where('dt_midia_incl', 'like', $parametros['data_inicio'].'%')
						->orderBy('dt_midia_incl', 'desc')
						->simplePaginate(12);
		} else if (array_key_exists("data_inicio", $parametros) && array_key_exists("data_fim", $parametros)) {
			$midias = Midia::where($where)
						//->where(['cd_midia_h' => '80', 'cd_midia_w' => '80'])
						->where('dt_midia_incl', 'like', $parametros['data_inicio'].'%')
						->where('dt_midia_incl', 'like', $parametros['data_fim'].'%')
						->orderBy('dt_midia_incl', 'desc')
						->simplePaginate(12);
		} else {
			$midias = Midia::where($where)
						//->where(['cd_midia_w' => '700', 'cd_midia_h' => '470'])
						->where("ds_midia_tag_orig", "like", $parametros['titulo'])
						->orderBy('dt_midia_incl', 'desc')
						->simplePaginate(12);
		}

		return $midias;
	}

	public function verificarTotalMidias($where) {
		$this->totalMidias = Midia::where($where)->count();
	}

}