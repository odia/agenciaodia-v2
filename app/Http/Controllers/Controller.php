<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Service\MidiaService;

class Controller extends BaseController {
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

	public function __construct() {
        //$this->middleware('guest')->except('logout'); 
    }

    public function index(Request $request) {
        $midiaService = new MidiaService();
        $ultimasMidias = $midiaService->ultimasMidiasCadastradas();
        
        if (Auth::user()) {
        	return view('home')->with(['midias' => $ultimasMidias, 'user' => Auth::user()]);
        }

        return view('home')->with(['midias' => $ultimasMidias]);
    }
    
}
