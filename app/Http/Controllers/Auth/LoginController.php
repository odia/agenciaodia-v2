<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\URL;
class LoginController extends Controller {
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->redirectTo = URL::previous();
        $this->middleware('guest')->except('logout');
    }

    protected function create(array $data) {
        return User::create([
            'ds_usuas_email' => $data['email'],
            'cd_usuas_senha' => md5($data['password'])
        ]);
    }

    protected function validateLogin(Request $request) {
        $this->validate($request, [
            $this->username() => 'required', 'cd_usuas_senha' => 'required',
        ]);
    }

    protected function credentials(Request $request) {
        return $request->only($this->username(), 'cd_usuas_senha');
    }

    public function username() {
        return 'ds_usuas_email';
    }

    public function authenticate(Request $request) {
        $email = $request->input("email");
        //$password = Hash::make($request->input("password"));
        $password = $request->input("password");
        $hashed = md5($password);

        $user = User::where([
            'ds_usuas_email' => $email,
            'cd_usuas_senha' => md5($password)
        ])->first();

        if ($user) {
            $this->guard()->login($user);
            //Auth::login($user);
            return redirect("/");
        } else {
            return redirect("/");
        }
    }

}
