<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Http\Service\MidiaService;

class MidiaController extends BaseController {

	private function instanciarMidiaService() {
		return new MidiaService();
	}

	public function listar() {
		$midiaService = $this->instanciarMidiaService();
		$midias = $midiaService->listarMidias();
		return view('midia/lista')->with(['midias' => $midias]);
	}

	public function pesquisarMidiasPorTitulo(Request $request) {
		$titulo = $request->input('titulo');
		$midiaService = $this->instanciarMidiaService();
		$midias = $midiaService->pesquisarMidiasPorTitulo($titulo);
		$totalMidias = $midiaService->totalMidias;
		
		$page = 1;
		if ($request->page != null && $request->page != '') {
			$page = $request->page;
			//$page++;
		}
		
		$user = Auth::user();
		if ($user) {
			return view('midia/lista')->with(['midias' => $midias, 'total' => $totalMidias, 'page' => $page, 'titulo' => $titulo, 'user' => $user]);
		}
		
		return view('midia/lista')->with(['midias' => $midias, 'total' => $totalMidias, 'page' => $page, 'titulo' => $titulo]);
	}
	
	public function pesquisarMidias(Request $request) {
		$titulo = $request->titulo;
		$produto = $request->produto;
		$tipo_midia = $request->tipo_midia;
		$data_inicio = $request->data_inicio;
		$data_fim = $request->data_fim;

		$parametros = array();
		$parametros['titulo'] = $titulo;

		if ($produto != "") {
			$parametros['produto'] = $produto;
		}
		if ($tipo_midia != "") {
			$parametros['tipo_midia'] = $tipo_midia;
		}
		if ($data_inicio != "") {
			$inicio = date_create($data_inicio);
			$parametros['data_inicio'] = date_format($inicio, 'Y-m-d');
		}
		if ($data_fim != "") {
			$fim = date_create($data_fim);
			$parametros['data_fim'] = date_format($fim, 'Y-m-d');
		}

		$midiaService = $this->instanciarMidiaService();
		$midias = $midiaService->pesquisarMidias($parametros);
		
		$page = 1;
		if ($request->page != null && $request->page != '') {
			$page = $request->page;
			//$page++;
		}

		$user = Auth::user();
		if ($user) {
			return view('midia/lista')->with(['midias' => $midias, 'total' => count($midias), 'page' => $page, 'titulo' => $titulo, 'user' => $user]);
		}

		return view('midia/lista')->with(['midias' => $midias, 'total' => count($midias), 'page' => $page, 'titulo' => $titulo]);
	}

	public function downloadDaMidia(Request $request) {
	    $path = $request->foto;
	    $nome = $request->nome;
	    header("Content-Type: image/jpg"); // informa o tipo do arquivo ao navegador
		header("Content-Disposition: attachment; filename=".$nome); // informa ao navegador que é tipo anexo e faz abrir a janela de download, tambem informa o nome do arquivo
		readfile($path);
	}
	
	public function verFoto(Request $request) {
		$utlimaURL = $_SERVER['HTTP_REFERER'];
		
		$user = Auth::user();
		if ($user) {
			return view("midia/foto")->with(["urlFoto" => $request->foto, "nomeFoto" => $request->nome, "descFoto" => $request->desc, "nomeArquivo" => $request->arq, "voltar" => $utlimaURL, "user" => $user]);
		}
		return view("midia/foto")->with(["urlFoto" => $request->foto, "nomeFoto" => $request->nome, "descFoto" => $request->desc, "nomeArquivo" => $request->arq, "voltar" => $utlimaURL]);
	}

}