## Instruçōes para rodar o projeto AGENCIA O DIA

Dependencias para levantar o sistema:

* Servidor (Apache ou Nginx)
* PHP >= 7.1.3
* Composer >= 1.7
* Laravel 5.7 

## Configurar o ambiente:

Caso não tenha o Composer e/ou o Larevel instalado, siga o passo a passo para [Instalar o Composer e Laravel](https://bitbucket.org/odia/agenciaodia-v2/wiki/Instalar%20o%20Laravel)

## Clonar o projeto:
Clonando o projeto: `git clone https://seu_usuario@bitbucket.org/odia/agenciaodia-v2.git`

## Gerar uma APP_KEY
Entre na pasta do projeto via terminal

Execute o comando `touch .env` no diretório raiz do projeto via terminal para criar o arquivo .env

Crie um KEY com o comando `php artisan key:generate` 

Copie e cole essa key dentro do arquivo .env na constante APP_KEY 

Configurar dentro de config/app.php

`'timezone' => 'America/Sao_Paulo'`

`'locale' => 'pt-br'`

Dar permissão de gravação na pasta cache ou o laravel não irá rodar: `sudo chmod 777 bootstrap/cache`

## Instalar as dependências:

Instalando as dependências: `composer update --no-scripts`

## Configurar o Banco de Dados:

Veja como configurar o banco de dados na Wiki do projeto: [Configurar o Banco](https://bitbucket.org/odia/agenciaodia-v2/wiki/Configurar-o-banco)

## Rodar os Testes:
Caso ainda não tenha o PHPUnit instalado, veja na Wiki do projeto como instlar o PHPUnit: [Intalar o PHPUnit](https://bitbucket.org/odia/agenciaodia-v2/wiki/instalar-phpunit)

Para executar os testes execute o comando: `phpunit tests/`

## Rodar o servidor:

Rodando o servidor: `php artisan serve --host=0.0.0.0 --port=8000`

No browser acesse: `http://localhost:8000`
