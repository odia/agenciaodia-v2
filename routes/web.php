<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//use App\Http\Controllers\MidiaController;
/*
Route::get('/', function () {
    return view('home');
});
*/
Route::get('download', 'MidiaController@downloadDaMidia');
Route::get('midias', 'MidiaController@listar');
Route::get('pesquisarMidia', 'MidiaController@pesquisarMidias');
Route::get('pesquisarMidiasPorTitulo', 'MidiaController@pesquisarMidiasPorTitulo');
Route::get('verfoto', 'MidiaController@verFoto');

Route::get('/', 'Controller@index');
Route::get('/home', 'HomeController@index')->name('home');

Route::post('/autenticar', 'Auth\LoginController@authenticate');

Auth::routes();
